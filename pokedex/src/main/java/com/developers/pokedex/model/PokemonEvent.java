package com.developers.pokedex.model;

import java.util.Objects;

public class PokemonEvent {

    private Long eventId;
    private String eventType;


    public PokemonEvent(Long value, String string) {
    }

    public Long getEventId() {
        return this.eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getEventType() {
        return this.eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PokemonEvent)) {
            return false;
        }
        PokemonEvent pokemonEvent = (PokemonEvent) o;
        return Objects.equals(eventId, pokemonEvent.eventId) && Objects.equals(eventType, pokemonEvent.eventType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId, eventType);
    }

    @Override
    public String toString() {
        return "{" +
            " eventId='" + getEventId() + "'" +
            ", eventType='" + getEventType() + "'" +
            "}";
    }

}