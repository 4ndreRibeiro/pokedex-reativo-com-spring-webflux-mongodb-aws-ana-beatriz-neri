package com.developers.pokedex.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Pokemon {

    @Id
    private String id;

    private String nome;
    private String categria;
    private String habilidade;
    private Double peso;

    public Pokemon(String id, String nome, String categria, String habilidade, Double peso) {
        this.id = id;
        this.nome = nome;
        this.categria = categria;
        this.habilidade = habilidade;
        this.peso = peso;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categria;
    }

    public void setCategoria(String categria) {
        this.categria = categria;
    }

    public String getHabilidade() {
        return habilidade;
    }

    public void setHabilidade(String habilidade) {
        this.habilidade = habilidade;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokemon pokemon = (Pokemon) o;
        return Objects.equals(id, pokemon.id) && Objects.equals(nome, pokemon.nome) && Objects.equals(categria, pokemon.categria) && Objects.equals(habilidade, pokemon.habilidade) && Objects.equals(peso, pokemon.peso);
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "id='" + id + '\'' +
                ", nome='" + nome + '\'' +
                ", categria='" + categria + '\'' +
                ", habilidade='" + habilidade + '\'' +
                ", peso=" + peso +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, categria, habilidade, peso);


    }
}
